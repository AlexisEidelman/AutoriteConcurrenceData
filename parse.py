# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 20:29:47 2016

@author: aeidelman
"""


from bs4 import BeautifulSoup
import http
import urllib.request

opener = urllib.request.FancyURLopener({})

page1 = 'http://www.autoritedelaconcurrence.fr/user/avisdec.php?numero=16-D-17'
f = opener.open(page1)
content = f.read()

def page(extension):
    url = 'http://www.autoritedelaconcurrence.fr/user/avisdec.php?numero=' + \
        extension
    f = opener.open(url)
    content = f.read()    
    soup = BeautifulSoup(content, "lxml")

    def titre(soup):
        titre = soup.find('div',attrs={"class":u"titre-avis-fond"})
        elements = [x.text for x in titre.find_all('p')]
        assert len(elements) == 2
        assert len(elements[0]) == 7
        assert elements[0][2] == elements[0][4] == '-'
        return [x.text for x in titre.find_all('p')]

    def chapeau(soup):
        chapeau = soup.find('div',attrs={"id":u"chapeau-avis"})
        href = chapeau.find('a',attrs={"class":u"liensorangegras"}).attrs['href']
        import pdb
        pdb.set_trace()        
        output2 = dict(
            href=href
            )
        return output2


    output1 = titre(soup)    
    output2 = chapeau(soup)
    
    href = output2['href']
    assert href[:12] == '../pdf/avis/'
    assert href[12:17] == extension.lower().replace('-','')
    return output1, output2
    