#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on Mon Aug 22 20:29:47 2016

@author: aeidelman
"""

import os
import itertools
import urllib

opener = urllib.request.FancyURLopener({})

page1 = 'http://www.autoritedelaconcurrence.fr/user/avisdec.php?numero=16-D-17'
f = opener.open(page1)
content = f.read()

def download_page(extension):
    #create directory
    path = 'data/' + extension
    if not os.path.exists(path):
        os.mkdir(path)
    
    # test download pdf
    try:
        url_pdf = 'http://www.autoritedelaconcurrence.fr/pdf/avis/'
        url_pdf += extension.lower().replace('-','') + '.pdf'
        urllib.request.urlretrieve(url_pdf, os.path.join(path,'pdf'))
        
    except urllib.error.HTTPError:
        os.rmdir(path)
        print('On a un probleme pour le pdf ', url_pdf)
        raise

    # if pdf worked, download html
    url = 'http://www.autoritedelaconcurrence.fr/user/avisdec.php?numero=' + \
        extension
    f = opener.open(url)
    content = f.read().decode('cp1252')
    with open('data/' + extension + '/html', 'w') as file:
        file.write(content)


for year in range(16, 89, -1):
    for i in itertools.count(1):
        for letter in ('D', 'A'):
            extension = '{}-{}-{:0=2d}'.format(year, letter, i)
            print(extension) 
            try:
                download_page(extension)
            except urllib.error.HTTPError:
                continue
            else:
                break
        else:
            # Next year
            break
   